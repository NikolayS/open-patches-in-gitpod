const anchorLinks = document.getElementsByTagName("a");

for (let i = 0; i < anchorLinks.length; i++) {
  if (anchorLinks[i].href.endsWith(".patch")) {
    const encodedLink = encodeURIComponent(anchorLinks[i].href);
    const gitPodLink = `https://gitpod.io/#var=${encodedLink}/https://gitlab.com/NikolayS/postgres/tree/transaction_timeout-v4`;
    anchorLinks[i].style.color = "#428bca";

    anchorLinks[i].addEventListener("mouseenter", function () {
      const div = document.createElement("div");
      let parent = anchorLinks[i].parentElement;
      div.className = "gitpod-div";
      div.innerHTML = "Open in Gitpod";

      anchorLinks[i].style.position = "relative";
      anchorLinks[i].appendChild(div);

      while (parent.tagName !== "TABLE") {
        parent = parent.parentElement;
        parent.style.overflow = "visible";
      }

      div.addEventListener("click", function (e) {
        e.preventDefault();
        window.open(gitPodLink, "_blank");
      });

      document.addEventListener("mouseover", function (event) {
        if (
          event.clientY > anchorLinks[i].getBoundingClientRect().bottom + 30 ||
          event.clientX < anchorLinks[i].getBoundingClientRect().left ||
          event.clientX > anchorLinks[i].getBoundingClientRect().right ||
          event.clientY < anchorLinks[i].getBoundingClientRect().top
        ) {
          div.remove();
        }
      });
    });
  }
}
